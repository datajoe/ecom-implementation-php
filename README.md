# DataJoe
## E-commerce Implementation for PHP
This is the PHP implementation for embedding DataJoe E-commerce onto your site. In order to use this tool, you will need 
both your e-commerce instance UUID and a DataJoe API token. Both can be requested from our support team via email or 
phone: [support@datajoe.com](mailto:support@datajoe.com) :: [303-989-1300](tel:+01-303-989-1300)

## Install
----------
**Via Composer:**

``` bash
$ composer require datajoe/ecom-implementation-php
```

**Via Download:**

[Click here to download](https://bitbucket.org/datajoe/ecom-implementation-php/downloads/) and include in your project.

## Usage
--------
Simplest integration utilizing a require statement:
```php
require_once Vendor/Datajoe/src/Implementation.php;
echo (new Datajoe\Ecom\Implementation(‘{ECOM_UUID}’, ‘{API_TOKEN}’))
	->getHtml();
```

Remote authentication using the autoloader:
```php
use DataJoe\Ecom\Implementation;
if($user->isLoggedIn()){
    echo (new Implementation('{ECOM_UUID}', '{API_TOKEN}'))
        ->withAuthentication($user->email, $user->firstName, $user->lastName, $user->djAuthArr)
        ->getHtml();
}else{
    echo (new Implementation(‘{ECOM_UUID}’, ‘{API_TOKEN}’))
    	->getHtml();
}
```

Overriding the default client base uri (used to create application navigation links):
```php
use DataJoe\Ecom\Implementation;
echo (new Implemenation('{ECOM_UUID}', '{API_TOKEN}'))  
    ->withUri('https://YourBaseUri/ecom/')
    ->getHtml();
```
