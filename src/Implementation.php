<?php
/**
 * Created by PhpStorm.
 * User: jacobfogg
 * Date: 4/19/17
 * Time: 11:35 AM
 */

namespace DataJoe\Ecom;

class Implementation
{
    const VERSION = 'PHP_1';

    private $domainPrefix = 'insights';
    private $dataJoeUri;
    private $ecomUuid;
    private $apiToken;
    private $djSessionId;

    private $remoteAuthenticationArray;
    private $clientBaseUri;

    public function __construct($ecomUuid, $apiToken, $domainPrefix = '')
    {
        $this->setEcomUuid($ecomUuid);
        $this->setApiToken($apiToken);
        $this->setDomainPrefix($domainPrefix);
        $this->setSessionId();

        return $this;
    }

    public function getHtml()
    {
        return $this->request($this->getRequestUri());
    }


    public function withAuthentication($email, $firstName, $lastName, $djAuthArr)
    {
        // TODO: Add input validation
        $this->remoteAuthenticationArray = [
            'email' => $email,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'djAuthArr' => $djAuthArr,
        ];

        return $this;
    }

    public function withUri($clientBaseUri)
    {
        // TODO: Add input validation
        $this->clientBaseUri = $clientBaseUri;

        return $this;
    }

    private function getRequestUri()
    {
        $requestUri = $this->dataJoeUri .
            '?djEcomUUID=' . urlencode($this->ecomUuid) .
            '&djApiKey=' . urlencode($this->apiToken) .
            '&djImplementationVersion=' . urlencode(self::VERSION) .
            '&djTimestamp=' . urlencode(time()) .
            '&djSessionId=' . urlencode($this->djSessionId) .
            '&' . $_SERVER['QUERY_STRING'];

        if ($this->remoteAuthenticationArray) {
            $requestUri .= '&djRemoteAuthenticationJson=' . urlencode(json_encode($this->remoteAuthenticationArray));
        }

        if ($this->clientBaseUri) {
            $requestUri .= '&djClientBaseUri=' . urlencode($this->clientBaseUri);
        }

        return $requestUri;
    }

    private function setSessionId()
    {
        if (array_key_exists('djSessionId', $_COOKIE)) {
            $this->djSessionId = $_COOKIE['djSessionId'];
        }elseif(session_id() !== '') {
            $this->djSessionId = session_id();
        }else{
            $this->djSessionId = md5(openssl_random_pseudo_bytes(16));
        }
    }

    private function setDomainPrefix($domainPrefix)
    {
        // TODO: Add domain prefix validation logic
        if ($domainPrefix != '') {
            $this->domainPrefix = $domainPrefix;
        }
        $this->dataJoeUri = "https://{$this->domainPrefix}.datajoe.com/";
    }

    private function setEcomUuid($ecomUuid)
    {
        //TODO: Add input validation
        $this->ecomUuid = $ecomUuid;
    }

    private function setApiToken($apiToken)
    {
        //TODO: Add input validation
        $this->apiToken = $apiToken;
    }

    private function request($requestUri)
    {
        //TODO: Consider adding support for Guzzle as an optional dependency
        if (function_exists('curl_version')) {//checks if curl exists
            return $this->curlRequest($requestUri);
        } else {//falls back to file_get_contents
            return $this->fileGetContentsRequest($requestUri);
        }
    }

    private function curlRequest($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        return curl_exec($ch);
    }

    private function fileGetContentsRequest($url)
    {
        return file_get_contents($url);
    }
}