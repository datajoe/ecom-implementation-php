# Changelog

All notable changes to `Ecom-Implementation-php` will be documented in this file

## [1.0.6](https://bitbucket.org/datajoe/ecom-implementation-php/commits/d988dceced8aff23c3e74fe9d160eb9bc199b9a9) - 2019-01-07
- Removed a php 7.1 dependant function
- Update implementation to point toward production server

## [1.0.5](https://bitbucket.org/datajoe/ecom-implementation-php/commits/903a02dba3936fe4511576136ab7fe5a62d11153) - 2017-08-22
- Moved variable logic out of the constructor to their own methods 
- Changed http to https

## [1.0.4](https://bitbucket.org/datajoe/ecom-implementation-php/commits/62b1236669981ee38122e72dccae2064384d234c) - 2017-08-17
- Changed the position of the variable setting for the session id to the constructor, with a conditional.
- Fixed readme typo

## [1.0.3](https://bitbucket.org/datajoe/ecom-implementation-php/commits/1f275027a8c99e846c0193b84c4a586750e8d0eb) - 2017-07-11
- Fixed a minor syntax error
- Fixed require instruction

## [1.0.2](https://bitbucket.org/datajoe/ecom-implementation-php/commits/b803c296f5c2d5a5454f8c812bc33d36f729a0e4) - 2017-04-20
- Fixed syntax issue

## [1.0.1](https://bitbucket.org/datajoe/ecom-implementation-php/commits/edfe6838fc5fba1d427b7aae05591b8851788f2e) - 2017-04-20
- Fixed autoloader issue
- Added the version tag
- Fixed file formatting

## [1.0.0](https://bitbucket.org/datajoe/ecom-implementation-php/commits/eeabb73e15eb24a8efad4b1c1c60606cf3a32bab) - 2017-04-19

### Initial Commit - Added:
- src/Implementation.php
- tests/Implementation.php
- .gitignore
- CHANGELOG.md
- composer.json
- LICENSE
- phpunit.xml
- README.md